#include "stdafx.h"
#include "CommonFile.h"

CommonFile::CommonFile(TCHAR *fn, bool creatingNew)
{
	name = fn;
	h = INVALID_HANDLE_VALUE;

	if(creatingNew)
		h = CreateFile(fn, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_NEW, 0, 0);
	else
		h = CreateFile(fn, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);

	CommonFile(h, creatingNew);
}

CommonFile::CommonFile(HANDLE inputH)
{
	CommonFile(inputH, false);
}

CommonFile::CommonFile(HANDLE inputH, bool creatingNew)
{
	name = _T("mappingName");
	hMap = 0;
	pMap = 0;

	/*_tcscpy(name, fn);
	TCHAR *r = _tcschr(name, _T(':'));
	while(r)
	{
		*r = _T('_');
		r = _tcschr(r+1, _T('\\'));
	}*/
	h = inputH;

	if(h != INVALID_HANDLE_VALUE)
	{
		DWORD size = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, size, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, size);
		}
	}
}

CommonFile::CommonFile(){}

CommonFile::~CommonFile()
{
	
}

PVOID CommonFile::GetAddr()
{
	return pMap;
}

void CommonFile::Destroy()
{
	if(pMap)
		UnmapViewOfFile(pMap);
	if(hMap)
		CloseHandle(hMap);
	if(h)
		CloseHandle(h);
}