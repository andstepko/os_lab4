// Lab4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <string.h>
#include <stdlib.h>

#include "CommonFile.h"

using std::string;
using std::cin;


HANDLE CreateBox(TCHAR fileName[], int maxBoxSize)
{
	HANDLE h1 = INVALID_HANDLE_VALUE;
	HANDLE hMap;
	LPVOID pMap;
	DWORD dwSize;
	bool b = true;
	TCHAR fullPath[100] = _T("d:\\mail\\");
	TCHAR extension[] = _T(".txt.");
	DWORD zero = 0;
	DWORD written = 0;
	//DWORD maxSize = MAXDWORD;
	BYTE* bMem;
	BYTE* bCurMem;

	_tcscat(fullPath, fileName);
	_tcscat(fullPath, extension);

	h1 = CreateFile((LPCTSTR)fullPath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, CREATE_NEW, 0, 0);
	dwSize = 3 * sizeof(DWORD);

	if(h1 != INVALID_HANDLE_VALUE)
	{
		hMap = CreateFileMapping(h1, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		} 
		bCurMem = (BYTE*) pMap;
		// amount of messages == 0
		*(DWORD*)(bCurMem) = zero;
		
		// total size of messages == 0
		if(b)
		{
			bCurMem += sizeof(DWORD);
			*(DWORD*)(bCurMem) = zero;
		}
		if(b)
		{
			bCurMem += sizeof(DWORD);
			*(DWORD*)(bCurMem) = maxBoxSize;
		}
	}
	if(pMap)
	{
		UnmapViewOfFile(pMap);
		pMap = 0;
	}
	if(hMap)
	{
		CloseHandle(hMap);
		hMap = 0;
	}

	return h1;
};
HANDLE CreateBox(TCHAR fileName[])
{
	DWORD maxSize = 65535;
	return CreateBox(fileName, maxSize);
}

HANDLE OpenBox(TCHAR fileName[])
{
	HANDLE h = INVALID_HANDLE_VALUE;
	bool b = false;
	TCHAR fullPath[100] = _T("d:\\mail\\");
	TCHAR extension[] = _T(".txt.");

	_tcscat(fullPath, fileName);
	_tcscat(fullPath, extension);

	h = CreateFile((LPCTSTR)fullPath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);

	return h;
}

bool AddLetter(HANDLE &h, TCHAR inputLetter[])
{
	bool b = true;
	DWORD dwCount;
	DWORD dwLetterSize = _tcslen(inputLetter) * sizeof(TCHAR);
	DWORD dwFileSize = GetFileSize(h, 0);
	DWORD dwMaxSize;
	DWORD dwSize;

	HANDLE hMap;
	LPVOID pMap;

	if(h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0) + sizeof(DWORD) + dwLetterSize;

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	BYTE *bMem = (BYTE*) pMap;
	BYTE *bCurMem = bMem + 8;
	dwMaxSize = *(DWORD*)(bCurMem);

	if(b && ((dwFileSize + dwLetterSize) <= dwMaxSize))
	{
		// Adding is permitted.
		DWORD dwLetterCount;
		bCurMem = bMem;
		dwLetterCount = *(DWORD*)(bCurMem);
		if(b)
		{
			// Change value of LetterCount.
			dwLetterCount++;
			*(DWORD*)(bCurMem) = dwLetterCount;
			bCurMem += sizeof(DWORD);

			if(b)
			{
				DWORD dwTotalSize;
				dwTotalSize = *(DWORD*)bCurMem;
				if(b)
				{
					// Change value of TotalSize.
					dwTotalSize += dwLetterSize + sizeof(DWORD);
					*(DWORD*)(bCurMem) = dwTotalSize;
					if(b)
					{
						// Write the exact letter.
						bCurMem = bMem + dwSize - dwLetterSize - sizeof(DWORD);
						*(DWORD*)(bCurMem) = dwLetterSize;
						bCurMem += sizeof(DWORD);
						_tcscpy((TCHAR*)(bCurMem), inputLetter);
					}
				}
			}
		}
		if(pMap)
		{
			UnmapViewOfFile(pMap);
			pMap = 0;
		}
		if(hMap)
		{
			CloseHandle(hMap);
			hMap = 0;
		}
	}
	return b;
};

TCHAR* OpenLetter(HANDLE h, DWORD index)
{
	bool b = true;
	TCHAR result[64*1024];

	DWORD dwLetterCount;
	DWORD dwCount = 0;

	HANDLE hMap;
	LPVOID pMap;
	DWORD dwSize;

	if(h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	b = true;
	BYTE *bMem = (BYTE*) pMap;
	BYTE *bCurMem = bMem;

	dwLetterCount = *(DWORD*)bCurMem;
	if(b && (dwLetterCount > index))
	{
		DWORD dwLetterLength;

		bCurMem += 3 * sizeof(DWORD);
		for(DWORD i = 0; i < index; i++)
		{
			if(b)
			{
				dwLetterLength = *(DWORD*)bCurMem;
				bCurMem += sizeof(DWORD);
				bCurMem += dwLetterLength;
			}
			else
				break;
		}			
		dwLetterLength = *(DWORD*)bCurMem;
		bCurMem += sizeof(DWORD);
		_tcsncpy(result, (TCHAR*)(bCurMem), dwLetterLength + 1);
		if(b)
		{
			result[dwLetterLength / sizeof(TCHAR)] = '\0';
			return result;
		}
		else
		{
			return _T("");
		}
	}
	else
	{
		return _T("");
	}
}

bool DeleteLetter(HANDLE h, DWORD index)
{
	bool b = true;
	DWORD dwLetterCount;
	DWORD dwLetterLength;
	DWORD dwTotalSize;
	DWORD dwCount;
	DWORD dwStartsWith = sizeof(DWORD) * 3;
	BYTE buf[64 * 1024];
	HANDLE hMap;
	LPVOID pMap;
	BYTE* bMem;
	BYTE* bCurMem;
	DWORD dwSize;

	if(h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	bMem = (BYTE*) pMap;
	bCurMem = bMem;

	dwLetterCount = *(DWORD*)bCurMem;
	if(b)
	{
		bCurMem += sizeof(DWORD);
		dwTotalSize = *(DWORD*)bCurMem;
	}
	if(b && (index < dwLetterCount))
	{
		// Removal allowed.		
		// Start looking for particular message.
		bCurMem = bMem + 3 * sizeof(DWORD);
		for(DWORD i = 0; i < index; i++)
		{
			if(b)
			{
				dwLetterLength = *(DWORD*)bCurMem;
				bCurMem += sizeof(DWORD) + dwLetterLength;
				dwStartsWith += sizeof(DWORD) + dwLetterLength;
			}
			else
				break;
		}
		if(b)
		{
			// Start removing of the particular letter.
			dwLetterLength = *(DWORD*)bCurMem;
			bCurMem += sizeof(DWORD) + dwLetterLength;
			if(b)
			{
				// Read the tail of file into buffer.
				memcpy(buf, bCurMem, dwSize - dwStartsWith);
				if(b)
				{
					// Write the tail instead of particular message.
					// Particular message starts with dwStartsWith
					bCurMem = bMem + dwStartsWith;
					memcpy(bCurMem, buf, dwSize - dwStartsWith);
					
					if(b)
					{
						// Decrease LetterCount.
						dwLetterCount--;
						bCurMem = bMem;
						*(DWORD*) bCurMem = dwLetterCount;
						if(b)
						{
							// Decrease TotalSize of messages by dwLetterLength
							dwTotalSize -= dwLetterLength;
							bCurMem += sizeof(DWORD);
							*(DWORD*) bCurMem = dwTotalSize;
						}
					}
					bCurMem += dwSize - dwStartsWith;
					SetFilePointer(h, bCurMem - bMem, 0, FILE_BEGIN);
					SetEndOfFile(h);
				}
			}
		}
		if(pMap)
		{
			UnmapViewOfFile(pMap);
			pMap = 0;
		}
		if(hMap)
		{
			CloseHandle(hMap);
			hMap = 0;
		}
		return b;
	}
	else
	{
		return false;
	}
}

TCHAR* OpenDeleteLetter(HANDLE h, DWORD index)
{
	bool b = true;
	TCHAR result[64*1024];
	DWORD dwLetterCount;
	DWORD dwCount = 0;
	DWORD dwSize;
	HANDLE hMap;
	BYTE* bMem;
	BYTE* bCurMem;
	LPVOID pMap;

	if(h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	bMem = (BYTE*) pMap;
	bCurMem = bMem;

	dwLetterCount = *(DWORD*)bCurMem;
	if(b && (dwLetterCount > index))
	{
		DWORD dwLetterLength;

		bCurMem = bMem + 3 * sizeof(DWORD);
		for(DWORD i = 0; i < index; i++)
		{
			dwLetterLength = *(DWORD*)bCurMem;
			bCurMem += sizeof(DWORD) + dwLetterLength;
		}		
		dwLetterLength = *(DWORD*)bCurMem;
		bCurMem += sizeof(DWORD);
		memcpy(result, bCurMem, dwLetterLength + 1);
		
		if(pMap)
		{
			UnmapViewOfFile(pMap);
			pMap = 0;
		}
		if(hMap)
		{
			CloseHandle(hMap);
			hMap = 0;
		}

		if(b)
		{
			b = DeleteLetter(h, index);
			if(b)
			{
				result[dwLetterLength] = '\0';
				return result;
			}
			else
				return _T("");
		}
		else
		{
			return _T("");
		}
	}
	else
	{
		return _T("");
	}
}

bool DeleteAllLetters(HANDLE h)
{
	bool b = true;
	DWORD dwZero = 0;
	DWORD dwCount;
	HANDLE hMap;
	LPVOID pMap;
	BYTE* bMem;
	BYTE* bCurMem;
	DWORD dwSize;

	if(h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	bMem = (BYTE*) pMap;
	bCurMem = bMem;

	// Set LetterCount to 0.
	*(DWORD*)(bCurMem) = dwZero;
	if(b)
	{
		// Set TotalSize to 0.
		bCurMem += sizeof(DWORD);
		*(DWORD*)(bCurMem) = dwZero;
		if(b)
		{
			// Remove all the messages.
			SetFilePointer(h, sizeof(DWORD) * 3, 0, FILE_BEGIN);
			SetEndOfFile(h);
		}
	}
	if(pMap)
	{
		UnmapViewOfFile(pMap);
		pMap = 0;
	}
	if(hMap)
	{
		CloseHandle(hMap);
		hMap = 0;
	}
	return b;
}

DWORD NumberOfLetters(HANDLE h)
{
	bool b = true;
	DWORD dwResult;
	DWORD dwCount;
	HANDLE hMap;
	LPVOID pMap;
	BYTE* bMem;
	BYTE* bCurMem;
	DWORD dwSize;

	if(h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	bMem = (BYTE*) pMap;
	bCurMem = bMem;

	dwResult = *(DWORD*)bCurMem;

	if(pMap)
	{
		UnmapViewOfFile(pMap);
		pMap = 0;
	}
	if(hMap)
	{
		CloseHandle(hMap);
		hMap = 0;
	}
	
	return (DWORD)dwResult;
}

DWORD CheckSum(HANDLE h)
{
	bool b = true;
	DWORD dwResult = 0;
	DWORD dwCurrentWord;
	DWORD dwCount;
	HANDLE hMap;
	LPVOID pMap;
	BYTE* bMem;
	BYTE* bCurMem;
	DWORD dwSize;

	if(h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READWRITE, 0, dwSize, 0);
		if(hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	bMem = (BYTE*) pMap;
	bCurMem = bMem;

	for(DWORD i = 0; i < (dwSize / sizeof(DWORD)); i++)
	{
		if(b)
		{
			dwCurrentWord = *(DWORD*)bCurMem;
			bCurMem += sizeof(DWORD);
			dwResult = (dwResult + dwCurrentWord) % MAXDWORD;
		}
		else
		{
			break;
		}
	}

	return dwResult;
}

void ShowDialog(HANDLE &h)
{
	TCHAR* answer = new TCHAR[5];

	system("cls");
	DWORD ooo = 9;
	bool handleBool = GetHandleInformation(h, &ooo);
	if((handleBool) && (h != INVALID_HANDLE_VALUE))
	{
		_tprintf(_T("A file is currently open.\r\n\r\n"));
	}
	else
	{
		// File is closed.
		_tprintf(_T("THERE IS NO OPENED FILE\r\n\r\n"));
	}
	_tprintf(_T("Create new box - 1\r\n"));
	_tprintf(_T("Open existing box - 2\r\n"));
	_tprintf(_T("Close current box - 3\r\n"));
	_tprintf(_T("Add a letter - 4\r\n"));
	_tprintf(_T("Open a letter - 5\r\n"));
	_tprintf(_T("Open and delete a letter - 6\r\n"));
	_tprintf(_T("Delete certain letter - 7\r\n"));
	_tprintf(_T("Delete all letters - 8\r\n"));
	_tprintf(_T("Check sum - 9\r\n"));
	_tprintf(_T("Number of letters - 10\r\n"));

	answer[0] = 'a';
	answer[1] = 'a';
	_tscanf_s(_T("%s"), answer, 5);

	system("cls");
	if((answer[0] == '1') && (answer[1] == '\0'))
	{
		// Create new box.
		TCHAR fileName[100];

		_tprintf(_T("Creating new file.\r\n"));
		_tprintf(_T("Enter file name:\r\n"));
		_tscanf_s(_T("%s"), fileName, _countof(fileName));

		DWORD ooo = 9;
		bool handleBool = GetHandleInformation(h, &ooo);
		if(handleBool)
			CloseHandle(h);
		//bool b = CreateBox(fileName, 65...);
		h = CreateBox(fileName);
	}
	else if(answer[0] == '2')
	{
		// Open existing box.
		TCHAR fileName[100];

		_tprintf(_T("Opening existing file.\r\n"));
		_tprintf(_T("Enter file name:\r\n"));
		_tscanf_s(_T("%s"), fileName, _countof(fileName));
		h = OpenBox(fileName);
	}
	else if(answer[0] == '3')
	{
		bool b = false;

		b = CloseHandle(h);
		if(b)
		{
			_tprintf(_T("success"));
		}
		else
		{
			_tprintf(_T("error"));
		}
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else if(answer[0] == '4')
	{
		// Add a letter.
		TCHAR letter[30000];

		_tprintf(_T("Type your letter:\r\n"));
		_tscanf_s(_T("%s"), letter, _countof(letter));

		bool b = AddLetter(h, letter);
		if(b)
			_tprintf(_T("success"));
		else
			_tprintf(_T("error"));
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else if(answer[0] == '5')
	{
		// Open a letter.
		DWORD index;
		LPCTSTR letter;
		TCHAR reading[2];

		_tprintf(_T("Enter the index of the letter you want to open:\r\n"));
		_tscanf_s(_T("%s"), reading, _countof(reading));
		index = _ttoi(reading);
		letter = OpenLetter(h, index);

		system("cls");
		_tprintf(letter);
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else if(answer[0] == '6')
	{
		// Open and delete a letter.
		DWORD index;
		LPCTSTR letter;
		TCHAR reading[2];

		_tprintf(_T("Enter the index of the letter you want to open and delete:\r\n"));
		_tscanf_s(_T("%s"), reading, _countof(reading));
		index = _ttoi(reading);
		letter = OpenDeleteLetter(h, index);

		system("cls");
		_tprintf(letter);
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else if(answer[0] == '7')
	{
		// Delete certain letter.
		bool b = false;
		DWORD index;
		LPCTSTR letter;
		TCHAR reading[2];

		_tprintf(_T("Enter the index of the letter you want to delete:\r\n"));
		_tscanf_s(_T("%s"), reading, _countof(reading));
		index = _ttoi(reading);
		b = DeleteLetter(h, index);

		system("cls");
		if(b)
			_tprintf(_T("success"));
		else
			_tprintf(_T("error"));
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else if(answer[0] == '8')
	{
		// Delete all letters.
		bool b;

		b = DeleteAllLetters(h);
		if(b)
			_tprintf(_T("success"));
		else
			_tprintf(_T("error"));
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else if(answer[0] == '9')
	{
		// Check sum
		DWORD dwCheckSum = CheckSum(h);

		system("cls");

		_tprintf(_T("Check sum:\r\n"));
		_tprintf(_T("%d"), dwCheckSum);
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else if((answer[0] == '1') && (answer[1] == '0'))
	{
		// Number of letters.
		DWORD dwNumberOfLetters;

		dwNumberOfLetters = NumberOfLetters(h);

		_tprintf(_T("%d"), dwNumberOfLetters);
		_tprintf(_T("\r\n--press eny key to finish"));
		_getch();
	}
	else
	{}
}

int _tmain(int argc, TCHAR* argv[])
{
	HANDLE h = INVALID_HANDLE_VALUE;

	_tsetlocale(LC_ALL, _T("Russian"));

	while(true)
	{
		ShowDialog(h);
	}	
	_getch();
	return 0;
}