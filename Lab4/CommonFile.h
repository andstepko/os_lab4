#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <string.h>
#include <stdlib.h>

using std::string;
using std::cin;

class CommonFile
{
public:
	PVOID pMap;
	HANDLE hMap;

private:
	HANDLE h;
	TCHAR* name;	

public:
	CommonFile(TCHAR *fn, bool creatingNew);
	CommonFile::CommonFile(HANDLE inputH);
	CommonFile::CommonFile(HANDLE inputH, bool creatingNew);
	CommonFile();
	PVOID GetAddr();
	~CommonFile();
	void Destroy();
};